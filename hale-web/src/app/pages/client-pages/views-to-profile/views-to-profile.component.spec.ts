import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewsToProfileComponent } from './views-to-profile.component';

describe('ViewsToProfileComponent', () => {
  let component: ViewsToProfileComponent;
  let fixture: ComponentFixture<ViewsToProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewsToProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewsToProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
