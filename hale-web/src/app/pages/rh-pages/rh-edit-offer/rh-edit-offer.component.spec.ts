import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RhEditOfferComponent } from './rh-edit-offer.component';

describe('RhEditOfferComponent', () => {
  let component: RhEditOfferComponent;
  let fixture: ComponentFixture<RhEditOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RhEditOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RhEditOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
