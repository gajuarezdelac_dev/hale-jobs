import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RhExamsComponent } from './rh-exams.component';

describe('RhExamsComponent', () => {
  let component: RhExamsComponent;
  let fixture: ComponentFixture<RhExamsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RhExamsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RhExamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
