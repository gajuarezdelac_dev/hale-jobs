import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RhViewExamByOfferComponent } from './rh-view-exam-by-offer.component';

describe('RhViewExamByOfferComponent', () => {
  let component: RhViewExamByOfferComponent;
  let fixture: ComponentFixture<RhViewExamByOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RhViewExamByOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RhViewExamByOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
