import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RhPaymentsComponent } from './rh-payments.component';

describe('RhPaymentsComponent', () => {
  let component: RhPaymentsComponent;
  let fixture: ComponentFixture<RhPaymentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RhPaymentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RhPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
