import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RhPreviewExamComponent } from './rh-preview-exam.component';

describe('RhPreviewExamComponent', () => {
  let component: RhPreviewExamComponent;
  let fixture: ComponentFixture<RhPreviewExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RhPreviewExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RhPreviewExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
