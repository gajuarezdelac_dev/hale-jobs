import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RhNotificationsComponent } from './rh-notifications.component';

describe('RhNotificationsComponent', () => {
  let component: RhNotificationsComponent;
  let fixture: ComponentFixture<RhNotificationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RhNotificationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RhNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
