import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RhViewCoworkersComponent } from './rh-view-coworkers.component';

describe('RhViewCoworkersComponent', () => {
  let component: RhViewCoworkersComponent;
  let fixture: ComponentFixture<RhViewCoworkersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RhViewCoworkersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RhViewCoworkersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
