import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RhViewPostulatesComponent } from './rh-view-postulates.component';

describe('RhViewPostulatesComponent', () => {
  let component: RhViewPostulatesComponent;
  let fixture: ComponentFixture<RhViewPostulatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RhViewPostulatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RhViewPostulatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
