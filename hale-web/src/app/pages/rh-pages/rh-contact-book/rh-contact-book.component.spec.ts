import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RhContactBookComponent } from './rh-contact-book.component';

describe('RhContactBookComponent', () => {
  let component: RhContactBookComponent;
  let fixture: ComponentFixture<RhContactBookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RhContactBookComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RhContactBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
