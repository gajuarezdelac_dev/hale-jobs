import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RhOffersComponent } from './rh-offers.component';

describe('RhOffersComponent', () => {
  let component: RhOffersComponent;
  let fixture: ComponentFixture<RhOffersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RhOffersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RhOffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
