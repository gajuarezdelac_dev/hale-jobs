import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RhHistoryComponent } from './rh-history.component';

describe('RhHistoryComponent', () => {
  let component: RhHistoryComponent;
  let fixture: ComponentFixture<RhHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RhHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RhHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
