import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminOpinionsByCompanyComponent } from './admin-opinions-by-company.component';

describe('AdminOpinionsByCompanyComponent', () => {
  let component: AdminOpinionsByCompanyComponent;
  let fixture: ComponentFixture<AdminOpinionsByCompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminOpinionsByCompanyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOpinionsByCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
