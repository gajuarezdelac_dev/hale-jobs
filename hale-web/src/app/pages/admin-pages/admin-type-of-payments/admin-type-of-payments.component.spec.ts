import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTypeOfPaymentsComponent } from './admin-type-of-payments.component';

describe('AdminTypeOfPaymentsComponent', () => {
  let component: AdminTypeOfPaymentsComponent;
  let fixture: ComponentFixture<AdminTypeOfPaymentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminTypeOfPaymentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTypeOfPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
