import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPostulationsComponent } from './admin-postulations.component';

describe('AdminPostulationsComponent', () => {
  let component: AdminPostulationsComponent;
  let fixture: ComponentFixture<AdminPostulationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminPostulationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPostulationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
