import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPostulationsIComponent } from './admin-postulations-i.component';

describe('AdminPostulationsIComponent', () => {
  let component: AdminPostulationsIComponent;
  let fixture: ComponentFixture<AdminPostulationsIComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminPostulationsIComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPostulationsIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
