import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminOffersIComponent } from './admin-offers-i.component';

describe('AdminOffersIComponent', () => {
  let component: AdminOffersIComponent;
  let fixture: ComponentFixture<AdminOffersIComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminOffersIComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOffersIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
