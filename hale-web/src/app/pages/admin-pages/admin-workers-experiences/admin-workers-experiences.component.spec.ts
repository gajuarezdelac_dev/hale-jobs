import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkersExperiencesComponent } from './admin-workers-experiences.component';

describe('AdminWorkersExperiencesComponent', () => {
  let component: AdminWorkersExperiencesComponent;
  let fixture: ComponentFixture<AdminWorkersExperiencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminWorkersExperiencesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkersExperiencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
