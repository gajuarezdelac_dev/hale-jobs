import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminReviewsByBusinessComponent } from './admin-reviews-by-business.component';

describe('AdminReviewsByBusinessComponent', () => {
  let component: AdminReviewsByBusinessComponent;
  let fixture: ComponentFixture<AdminReviewsByBusinessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminReviewsByBusinessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminReviewsByBusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
