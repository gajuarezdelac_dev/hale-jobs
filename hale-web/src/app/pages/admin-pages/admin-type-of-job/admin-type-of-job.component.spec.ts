import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTypeOfJobComponent } from './admin-type-of-job.component';

describe('AdminTypeOfJobComponent', () => {
  let component: AdminTypeOfJobComponent;
  let fixture: ComponentFixture<AdminTypeOfJobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminTypeOfJobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTypeOfJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
