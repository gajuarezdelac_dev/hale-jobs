import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AboutComponent } from './pages/global-pages/about/about.component';
import { AdminDashboardComponent } from './pages/admin-pages/admin-dashboard/admin-dashboard.component';
import { AdminUsersComponent } from './pages/admin-pages/admin-users/admin-users.component';
import { AuthComponent } from './pages/auth/auth.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { RecoveryPasswordComponent } from './pages/auth/recovery-password/recovery-password.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { HomeComponent } from './pages/global-pages/home/home.component';
import { MyProfileComponent } from './pages/client-pages/my-profile/my-profile.component';
import { SearchJobsComponent } from './pages/global-pages/search-jobs/search-jobs.component';
import { SearchOffersComponent } from './pages/global-pages/search-offers/search-offers.component';
import { TermsConditionsComponent } from './pages/global-pages/terms-conditions/terms-conditions.component';
import { ViewJobComponent } from './pages/global-pages/view-job/view-job.component';
import { ViewOfferComponent } from './pages/global-pages/view-offer/view-offer.component';
import { ViewWorkerComponent } from './pages/global-pages/view-worker/view-worker.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';

const routes: Routes = [
  { path: 'dashboard', component: WelcomeComponent, children: [
     { path: "statisticts", component: AdminDashboardComponent },
     { path: "users", component: AdminUsersComponent },
     { path: '', pathMatch: 'full', redirectTo: '/login' },
    ],
  // canActivate: [ AuthGuard ]
  },
  {path: 'auth', component: AuthComponent, children : [
    { path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "recovery-password", component: RecoveryPasswordComponent },
  ],
  data: { animation: 'AuthPage' }
},
  {
    path: '', component: NavbarComponent, children: [
      { path: 'home', component: HomeComponent},
      { path: 'search', component: SearchOffersComponent}, 
      { path: 'search-jobs', component: SearchJobsComponent },
      { path: 'view-offer', component: ViewOfferComponent},
      { path: 'view-user/:id', component: ViewWorkerComponent },
      { path: 'view-job/:id', component: ViewJobComponent },
      { path: 'my-profile', component: MyProfileComponent }, 
      { path: 'view/:id', component: ViewOfferComponent },
      { path: 'about', component: AboutComponent },
      { path: "terms-and-conditions", component: TermsConditionsComponent },
      // { path: 'support', component: SupportComponent },
      { path: '', pathMatch: 'full', redirectTo: '/home' },
      
    ],
    data: { animation: 'HomePage' }
  },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
